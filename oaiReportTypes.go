package oaireporttypes

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

// ProcessingParams process state message
type ProcessingParams struct {
	ID           bson.ObjectId `bson:"_id,omitempty"`
	SessionID    string        `json:"session_id" bson:"session_id"`
	FunctionName string        `json:"name" bson:"name"`
	ProcessID    int32         `json:"process_id" bson:"process_id"`
	DelayBefore  int64         `json:"delay_before" bson:"delay_before"`
	DelayAfter   int64         `json:"delay_after" bson:"delay_after"`
}

// ProcessingResult process state message
type ProcessingResult struct {
	ID                    bson.ObjectId `bson:"_id,omitempty"`
	SessionID             string        `json:"session_id" bson:"session_id"`
	FunctionName          string        `json:"name" bson:"name"`
	PID                   int32         `json:"pid" bson:"pid"`
	ProcessingTime        int64         `json:"processing_time" bson:"processing_time"`
	ProcessingTimeMoments []int64       `json:"processing_time_moments" bson:"processing_time_moments"`
	ElapsedTime           int64         `json:"interval"`
	IntercallTime         int64         `json:"intercall_time" bson:"intercall_time"`
	TimeStamp             int64         `json:"timestamp"`
	DateTime              time.Time     `bson:"datetime"`
	Elapsed               int32         `json:"elapsed" bson:"elapsed,omitempty"`
}

// UeStateResult ue state message
type UeStateResult struct {
	ID                      bson.ObjectId       `bson:"_id,omitempty"`
	SessionID               string              `json:"session_id" bson:"session_id"`
	UEid                    int                 `json:"ue_id" bson:"ue_id"`
	RNTI                    int                 `json:"rnti" bson:"rnti"`
	WbCQI                   float32             `json:"wb_cqi" bson:"wb_cqi"`
	MCS                     float32             `json:"mcs" bson:"mcs"`
	MCSUl                   float32             `json:"mcs_ul" bson:"mcs_ul"`
	NbRb                    float32             `json:"nb_rb" bson:"nb_rb"`
	NbRbUl                  float32             `json:"nb_rb_ul" bson:"nb_rb_ul"`
	NbRbTotal               int64               `json:"nb_rb_total" bson:"nb_rb_total"`
	NbRbCnt                 int64               `json:"nb_rb_cnt" bson:"nb_rb_cnt"`
	MacThroughput           float32             `json:"mac_tp" bson:"mac_tp"`
	MacThroughputUl         float32             `json:"mac_tp_ul" bson:"mac_tp_ul"`
	TimeStamp               int64               `json:"timestamp"`
	DateTime                time.Time           `bson:"datetime"`
	Elapsed                 int32               `json:"elapsed" bson:"elapsed,omitempty"`
	HarqProcessStatusList   []HarqProcessStatus `json:"harq_buffer" bson:"harq_buffer"`
	HarqProcessStatusUlList []HarqProcessStatus `json:"harq_buffer_ul" bson:"harq_buffer_ul"`
	HarqProcessNackList     []HarqProcessStatus `json:"harq_buffer_nack" bson:"harq_buffer_nack"`
	HarqProcessNackUlList   []HarqProcessStatus `json:"harq_buffer_nack_ul" bson:"harq_buffer_nack_ul"`
}

// EnbStateResult enb state message
type EnbStateResult struct {
	ID           bson.ObjectId `bson:"_id,omitempty"`
	SessionID    string        `bson:"session_id" json:"session_id"`
	EnbID        int           `json:"enb_id" bson:"enb_id"`
	NrbDl        int           `json:"n_rb_dl" bson:"n_rb_dl"`
	FreqDl       int           `json:"freq_dl" bson:"freq_dl"`
	FreqUl       int           `json:"freq_ul" bson:"freq_ul"`
	TxGain       float32       `json:"tx_gain" bson:"tx_gain"`
	RxGain       float32       `json:"rx_gain" bson:"rx_gain"`
	MeasOverhead int64         `json:"meas_overhead" bson:"meas_overhead"`
	TimeStamp    int64         `json:"timestamp"`
	Uptime       int32         `json:"uptime" bson:"uptime,omitempty"`
	DateTime     time.Time     `bson:"datetime"`
	CreatedTs    int64         `json:"created_ts" bson:"created_ts"`
	CreatedDt    time.Time     `bson:"created_dt"`
	Elapsed      int32         `json:"elapsed" bson:"elapsed,omitempty"`
}

// EnbMessageT represent enb report message
type EnbMessageT struct {
	ProcessesStates []ProcessingResult `json:"processes,omitempty"`
	UeStates        []UeStateResult    `json:"ues,omitempty"`
	EnbStates       []EnbStateResult   `json:"enbs,omitempty"`
}

// DockerMessageT represent Docker report message
type DockerMessageT struct {
}

// Command represent control message
type Command struct {
	Name        string `json:"name,omitempty" bson:"name,omitempty"`
	Description string `json:"description,omitempty" bson:"description,omitempty"`
	Command     string `json:"command" bson:"command"`
	Value       string `json:"value,omitempty" bson:"value,omitempty"`
}

// HarqProcessStatus represent control message
type HarqProcessStatus struct {
	HarqPID    int   `json:"harq_pid" bson:"harq_pid"`
	BufferSize int64 `json:"buffer_size" bson:"buffer_size"`
	Count      int64 `json:"count" bson:"count"`
}

// ControlMessages represent control messages
type ControlMessages struct {
	Commands []Command `json:"commands"`
}

// ReportMessage represent group of all messages
type ReportMessage struct {
	EnbMessage      EnbMessageT     `json:"enb_message,omitempty"`
	DockerMessage   DockerMessageT  `json:"docker_message,omitempty"`
	ControlMessages ControlMessages `json:"control_messages,omitempty"`
	SessionID       string          `json:"session_id"`
	TimeStamp       int64           `json:"timestamp"`
}

// Session represent eNB session object
type Session struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	SessionID   string        `bson:"session_id" json:"session_id"`
	IP          string        `bson:"ip" json:"ip"`
	Port        string        `bson:"port" json:"port"`
	TimeStamp   int64         `json:"timestamp"`
	DateTime    time.Time     `bson:"datetime"`
	Trace       bool          `bson:"trace" json:"trace"`
	Description string        `bson:"description" json:"description"`
	Elapsed     int32         `json:"elapsed" bson:"elapsed,omitempty"`
}

// User represent session user object
type User struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	SessionID string        `bson:"session_id" json:"session_id"`
	UserID    string        `bson:"user_id" json:"user_id"`
	DateTime  time.Time     `bson:"datetime"`
	UserType  string        `bson:"user_type" json:"user_type"`
	Active    int           `bson:"active" json:"active"`
	Token     string        `bson:"token" json:"token"`
}

// Message represent user message object
type Message struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	TargetType  string        `bson:"target_type" json:"target_type"`
	TargetID    string        `bson:"target_id" json:"target_id"`
	SourceType  string        `bson:"source_type" json:"source_type"`
	SourceID    string        `bson:"source_id" json:"source_id"`
	Message     string        `bson:"message" json:"message"`
	MessageType string        `bson:"message_type" json:"message_type"`
	DateTime    time.Time     `bson:"datetime" json:"datetime"`
	Reference   string        `bson:"reference" json:"reference"`
}

// Test represent eNB test object
type Test struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	TestID         string        `bson:"test_id" json:"test_id"`
	EstimatedTime  int64         `bson:"estimated_time" json:"estimated_time"`
	TotalTime      int64         `bson:"total_time" json:"total_time"`
	Output         string        `bson:"output" json:"output"`
	Params         string        `bson:"params" json:"params"`
	Labels         string        `bson:"labels" json:"labels"`
	UserID         string        `bson:"user_id" json:"user_id"`
	ExitCode       int32         `bson:"exit_code" json:"exit_code"`
	RestartCounter int32         `bson:"restarted_counter" json:"restarted_counter"`
}

// SessionResp represent eNB session object response
type SessionResp struct {
	Session Session        `json:"session,omitempty"`
	Enb     EnbStateResult `json:"enb_state,omitempty"`
}

// UeState represent UE last state
type UeState struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	SessionID string        `json:"session_id" bson:"session_id"`
	UEid      int           `json:"ue_id" bson:"ue_id"`
	TimeStamp int64         `json:"timestamp"`
	DateTime  time.Time     `bson:"datetime"`
	PhyTP     float32       `json:"phy_tp" bson:"phy_tp"`
	DlBER     float32       `json:"dl_ber" bson:"dl_ber"`
	UlBER     float32       `json:"ul_ber" bson:"ul_ber"`
	RSRP      int32         `json:"rsrp" bson:"rsrp"`
	RSRQ      int32         `json:"rsrq" bson:"rsrq"`
	Result    UeStateResult `json:"result,omitempty" bson:"result,omitempty"`
	Params    UeParams      `json:"params,omitempty" bson:"params,omitempty"`
}

// ProcessingState represent process last state
type ProcessingState struct {
	ID           bson.ObjectId    `bson:"_id,omitempty"`
	SessionID    string           `json:"session_id" bson:"session_id"`
	FunctionName string           `json:"name" bson:"name"`
	FunctionInfo FunctionInfo     `json:"function_info" bson:"function_info,omitempty"`
	TimeStamp    int64            `json:"timestamp"`
	DateTime     time.Time        `bson:"datetime"`
	Result       ProcessingResult `json:"result,omitempty" bson:"result,omitempty"`
}

// UeParams represent UE parameters
type UeParams struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	SessionID string        `json:"session_id" bson:"session_id"`
	UEid      int           `json:"ue_id" bson:"ue_id"`
	MaxMCS    int           `json:"max_mcs" bson:"max_mcs"`
	MaxMCSUl  int           `json:"max_mcs_ul" bson:"max_mcs_ul"`
	MinMCS    int           `json:"min_mcs" bson:"min_mcs"`
	BWLow     int           `json:"bw_low" bson:"bw_low"`
	BWUp      int           `json:"bw_up" bson:"bw_up"`
	BWUlMax   int           `json:"bw_ul_max" bson:"bw_ul_max"`
	TimeAlloc int           `json:"time_alloc" bson:"time_alloc"`
}

// FunctionInfo represent addtional function info
type FunctionInfo struct {
	Direction string `json:"direction" bson:"direction"`
	Order     int    `json:"order" bson:"order"`
}

// ControlResponse represent control response struct
type ControlResponse struct {
	ProcessingParams []ProcessingParams `json:"processing_params"`
	UeParams         []UeParams         `json:"ue_params"`
}
